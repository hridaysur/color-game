var numSquare = 6;

var colors = generateColors(numSquare);

var squares = document.querySelectorAll(".square");

var pickedColor = pickColor();

var colorDisplay = document.querySelector("#colorDisplay");

colorDisplay.textContent = pickedColor;

var messageDisplay = document.querySelector("#messageDisplay");

var reset = document.querySelector("#reset");

var easyButton = document.querySelector("#easyButton");
var hardButton = document.querySelector("#hardButton");

var h1 = document.querySelector("h1");

for (var i= 0 ; i < squares.length;i++) {
	//color set
	squares[i].style.backgroundColor = colors[i];

	//click event

	squares[i].addEventListener("click", function() {
		// grab a color from the color array
		var clickedColor = this.style.backgroundColor;

		if (clickedColor === pickedColor) {
			messageDisplay.textContent="correct";
			h1.style.backgroundColor= clickedColor;
			reset.textContent = "Play Again";
			changeColors(pickedColor);

		}
		else{
			this.style.backgroundColor = "#232323";
			messageDisplay.textContent = "try again";
		}
	})
}

function changeColors(color) {
	// body...
	for(i = 0; i < squares.length; i++){
		squares[i].style.backgroundColor = color;
	}
}

function pickColor() {
	var noumber = Math.floor(Math.random() * colors.length);
	return colors[noumber];
}

function generateColors(num) {
	// body...
	//make an array
	var arr = [ ];
	//add random colors to an array
	for (var i = 0; i < num; i++) {
		//get random color and push into a arr
		arr.push(randomColor());
	}
	//return that array
	return arr;
}

function randomColor() {
//pick red from 0-255
var r = Math.floor(Math.random() * 256);

//pick color for green
var g = Math.floor(Math.random() * 256);

//pick fro blue
var b = Math.floor(Math.random() * 256);

//make a string
return "rgb(" + r + ", " + g + ", " + b +")";
	
}

reset.addEventListener("click",function () {

	//
	reset.textContent = "New Color";
	messageDisplay.textContent = "";
	//genrate all new color
	colors = generateColors(numSquare);

	//pick a new random color from array
	pickedColor = pickColor();

	//chnage coloDisplay to match picked color
	colorDisplay.textContent = pickedColor;

	//change colors for square
	for (var i = 0; i < squares.length; i++) {
		squares[i].style.backgroundColor = colors[i];
	}

	h1.style.backgroundColor= "steelblue";
})

easyButton.addEventListener("click" , function(){
easyButton.classList.add("selected");
hardButton.classList.remove("selected");
numSquare = 3;
colors = generateColors(numSquare);
pickedColor = pickColor();
colorDisplay.textContent = pickedColor;

for (var i = 0; i < squares.length; i++) {
	if(colors[i]){
		squares[i].style.backgroundColor = colors[i];
	}
	else{
		squares[i].style.display ="none";
	}
}

})

hardButton.addEventListener("click" , function(){
hardButton.classList.add("selected");
easyButton.classList.remove("selected");
colors = generateColors(numSquare);
pickedColor = pickColor();
colorDisplay.textContent = pickedColor;

for (var i = 0; i < squares.length; i++) {
	
		squares[i].style.backgroundColor = colors[i];
		squares[i].style.display ="block";
	
}
	
})